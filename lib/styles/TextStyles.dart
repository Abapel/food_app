import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

TextStyle appBarTitleStyle =
    TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 24);
