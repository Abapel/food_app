import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void showAcceptCancelDialog(
    BuildContext context,
    String title,
    String message,
    String acceptButtonText,
    String cancelButtonText,
    VoidCallback onAccepted,
    VoidCallback onCanceled) {
  showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title),
          content: Text(message),
          actions: <Widget>[
            FlatButton(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
                child: Text(
                  cancelButtonText,
                  style: TextStyle(fontSize: 20),
                ),
              ),
              onPressed: () {
                onCanceled();
                Navigator.pop(context);
              },
            ),
            RaisedButton(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
                child: Text(
                  acceptButtonText,
                  style: TextStyle(fontSize: 20, color: Colors.white),
                ),
              ),
              onPressed: () {
                onAccepted();
                Navigator.pop(context);
              },
            )
          ],
        );
      });
}

void showAcceptDialog(BuildContext context, String title, String message,
    String acceptButtonText, VoidCallback onAccepted) {
  showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title),
          content: Text(message),
          actions: <Widget>[
            FlatButton(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
                child: Text(
                  acceptButtonText,
                  style: TextStyle(fontSize: 20),
                ),
              ),
              onPressed: () {
                onAccepted();
                Navigator.pop(context);
              },
            ),
          ],
        );
      });
}
