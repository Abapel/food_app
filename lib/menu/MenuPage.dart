import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:food_app/api/FoodCategory.dart';
import 'package:food_app/api/FoodProduct.dart';
import 'package:food_app/api/FoodRestaurant.dart';
import 'package:food_app/database/ProductsTable.dart';
import 'package:food_app/events/CountChangedEvent.dart';
import 'package:food_app/events/EventManager.dart';
import 'package:food_app/styles/TextStyles.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:food_app/widgets/StatelessCounterWidget.dart';

class MenuPage extends StatefulWidget {
  FoodRestaurant restaurant;
  MenuPage(FoodRestaurant restaurant) {
    this.restaurant = restaurant;
  }

  @override
  MenuPageState createState() {
    return MenuPageState(restaurant);
  }
}

class MenuPageState extends State<MenuPage> {
  double rating = 3.5;
  List<FoodCategory> categories = <FoodCategory>[];
  List<FoodProduct> products = <FoodProduct>[];
  FoodRestaurant restaurant;

  MenuPageState(FoodRestaurant restaurant) {
    this.restaurant = restaurant;
  }

  bool categoryExist(FoodCategory category) {
    bool result = false;
    categories.forEach((each) {
      if (category.id == each.id) {
        result = true;
      }
    });
    return result;
  }

  void updateList() {
    parseCategories().then((newCategories) {
      if (!mounted) return;
      setState(() {
        categories = <FoodCategory>[];
        categories = newCategories;
      });
    });
  }

  @override
  void initState() {
    super.initState();

    Future.delayed(const Duration(milliseconds: 500), () {
      updateList();
    });

    EventManager.getInstance().eventBus.on<CountChangedEvent>().listen((event) {
      updateList();
    });
  }

  List<String> getCategoriesTitles() {
    List<String> titles = <String>[];
    for (int i = 0; i < categories.length; i++) {
      titles.add(categories[i].name);
    }
    return titles;
  }

  List<Widget> getTabsWithTitles(List<String> titles) {
    List<Widget> tabs = <Widget>[];
    for (int i = 0; i < titles.length; i++) {
      tabs.add(Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            titles[i],
            style: TextStyle(color: Colors.black, fontSize: 20),
          ),
        ],
      ));
    }
    if (tabs.length == 0) {
      tabs.add(Text(''));
      tabs.add(Text(''));
    }
    return tabs;
  }

  List<Widget> getTabPages() {
    List<Widget> pages = <Widget>[];
    for (int i = 0; i < categories.length; i++) {
      pages.add(getListViewForCategoryId(categories[i].id));
    }
    if (pages.length == 0) {
      pages.add(Center(
        child: CircularProgressIndicator(),
      ));
      pages.add(Center(
        child: CircularProgressIndicator(),
      ));
    }
    return pages;
  }

  int getPageCount() {
    List<String> titles = getCategoriesTitles();
    return titles.length > 0 ? titles.length : 2;
  }

  Widget getListViewForCategoryId(int categoryId) {
    return FutureBuilder<List<FoodProduct>>(
      future: getProductsWithCategoryId(categoryId),
      builder:
          (BuildContext context, AsyncSnapshot<List<FoodProduct>> snapshot) {
        if (snapshot.hasData) {
          return ListView.builder(
              itemCount: snapshot.data.length,
              itemBuilder: (context, index) {
                FoodProduct item = snapshot.data[index];
                String priceText = item.getPriceText();

                return Card(
                  elevation: 2,
                  margin: EdgeInsets.fromLTRB(16, 10, 16, 10),
                  child: Column(
                    children: <Widget>[
                      FadeInImage.assetNetwork(
                        placeholder: 'images/placeholder.png',
                        image: item.getImageUrl(),
                        height: 250,
                        width: MediaQuery.of(context).size.width,
                        fit: BoxFit.cover,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Container(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: <Widget>[
                              RichText(
                                text: TextSpan(
                                  children: <TextSpan>[
                                    TextSpan(
                                        text: item.name,
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 26,
                                            fontWeight: FontWeight.w500,
                                            fontFamily: 'Helvetica Neue')),
                                    TextSpan(
                                        text:
                                            ' ${snapshot.data[index].getWeight()}',
                                        style: new TextStyle(
                                            color: Colors.grey[500],
                                            fontSize: 26,
                                            fontWeight: FontWeight.w500,
                                            fontFamily: 'Helvetica Neue')),
                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.fromLTRB(0, 8, 0, 0),
                                child: Text(
                                  '${snapshot.data[index].description}',
                                  style: TextStyle(
                                      color: Colors.black, fontSize: 16),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.fromLTRB(0, 16, 0, 0),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text(priceText,
                                        style: TextStyle(fontSize: 24)),
                                    SizedBox(
                                      height: 50,
                                      child: Stack(
                                        children: <Widget>[
                                          Visibility(
                                            visible: item.getCount() > 0,
                                            child: StatelessCounterWidget(
                                                count: item.getCount(),
                                                size: 50,
                                                food: item,
                                                callback: () {
                                                  if (!mounted) return;
                                                  setState(() {
                                                    setOrderedProductWithErrorAlert(
                                                        context, item);
                                                    priceText =
                                                        item.getPriceText();
                                                  });
                                                }),
                                          ),
                                          Visibility(
                                            visible: item.getCount() == 0,
                                            child: RaisedButton(
                                              onPressed: () {
                                                if (!mounted) return;
                                                setState(() {
                                                  item.count++;
                                                  setOrderedProductWithErrorAlert(
                                                      context, item);
                                                });
                                              },
                                              textColor: Colors.white,
                                              child: Row(
                                                children: <Widget>[
                                                  Text(
                                                    'Заказать',
                                                    style: TextStyle(
                                                        fontSize: 16,
                                                        fontWeight:
                                                            FontWeight.w800),
                                                  )
                                                ],
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                );
              });
        } else {
          return Center(
            child: SpinKitCircle(
              color: Colors.black,
              size: 22,
              duration: Duration(milliseconds: 1000),
            ),
          );
        }
      },
    );
  }

  Future<List<FoodCategory>> parseCategories() async {
    List<FoodProduct> newProducts =
        await getProductsWithRestaurantId(restaurant.id);
    categories = <FoodCategory>[];
    products = newProducts;
    products.forEach((product) {
      if (!categoryExist(product.category)) {
        categories.add(product.category);
      }
    });
    for (int i = 0; i < categories.length; i++) {
      categories[i].products = getSavedProductsWithCategoryId(categories[i].id);
    }
    return categories;
  }

  List<FoodProduct> getSavedProductsWithCategoryId(int categoryId) {
    List<FoodProduct> result = <FoodProduct>[];
    for (int i = 0; i < products.length; i++) {
      if (products[i].categoryId == categoryId) {
        result.add(products[i]);
      }
    }
    return result;
  }

  @override
  Widget build(BuildContext context) {
    List<String> tabTitles = getCategoriesTitles();
    List<Widget> tabs = getTabsWithTitles(tabTitles);

    return SafeArea(
      child: Scaffold(
          body: Stack(
        children: <Widget>[
          AppBar(
            title: Text(
              restaurant.name,
              style: appBarTitleStyle,
            ),
            iconTheme: IconThemeData(
              color: Colors.black, //change your color here
            ),
            backgroundColor: Colors.white,
            elevation: 0,
            actions: <Widget>[
              InkWell(
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(8, 0, 16, 0),
                  child: Icon(
                    CupertinoIcons.phone_solid,
                    color: Colors.black,
                  ),
                ),
                onTap: () {
                  launch("tel://${restaurant.telephone}");
                },
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(0, kToolbarHeight, 0, 0),
            child: DefaultTabController(
              length: getPageCount(),
              child: new Scaffold(
                backgroundColor: Colors.white,
                appBar: TabBar(
                  unselectedLabelColor: Colors.grey,
                  indicatorColor: categories.length > 0
                      ? Theme.of(context).primaryColor
                      : Colors.transparent,
                  isScrollable: true,
                  tabs: tabs,
                ),
                body: AnimatedOpacity(
                    opacity: categories.length > 0 ? 1.0 : 0.0,
                    duration: Duration(milliseconds: 500),
                    child: Stack(
                      children: <Widget>[
                        TabBarView(
                          children: getTabPages(),
                        ),
                      ],
                    )),
              ),
            ),
          )
        ],
      )),
    );
  }
}
