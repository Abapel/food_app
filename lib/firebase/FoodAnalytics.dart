import 'package:firebase_analytics/firebase_analytics.dart';

FirebaseAnalytics analytics = FirebaseAnalytics();

const String bottomBarNavigationEvent = 'bottom_bar_navigation';
const String cityNavigationEvent = 'city_navigation';
const String creatingOrderEvent = 'creating_order';
const String creatingOrderErrorEvent = 'creating_order_error';
const String orderCreatedEvent = 'order_created';
const String useLocationEvent = 'use_location';
const String locationPermissionsGrantedEvent = 'location_permissions_granted';
const String locationPermissionsDeniedEvent = 'location_permissions_denied';
const String setProductCountEvent = 'product_count';

Future<void> trackEvent(String name) async {
  await trackEventWithParam(name, '');
}

Future<void> trackEventWithParam(String name, String customParam) async {
  await analytics.logEvent(
    name: name,
    parameters: <String, dynamic>{'custom_param': customParam},
  );
}

Future<void> trackProductCount(String productName, int count) async {
  await analytics.logEvent(
    name: setProductCountEvent,
    parameters: <String, dynamic>{'product_name': productName, "count": count},
  );
}
