import 'package:flutter/cupertino.dart';
import 'package:food_app/api/FoodProduct.dart';
import 'package:food_app/dialogs/DialogHelper.dart';

import 'ProductsTable.dart';

void showConfirmDeleteOrderedProductsDialog(BuildContext context) {
  showAcceptCancelDialog(context, 'Подтвердите действие',
      'Вы действительно хотите очистить корзину?', 'Очистить', 'Отмена', () {
    //onAccept
    deleteAllOrderedProducts();
  }, () {
    //onCancel
  });
}

void showConfirmDeleteOnConflictOrderedProductsDialog(
    BuildContext context, VoidCallback onConfirmed) {
  showAcceptCancelDialog(
      context,
      'Вы заказываете из другого ресторана',
      'Ваша корзина уже содержит товары из другого ресторана. Нельзя заказывать из разных ресторанов одновременно. Очистить корзину и добавить новый товар?',
      'Очистить',
      'Отмена', () async {
    //onAccept
    await deleteAllOrderedProducts();
    onConfirmed();
  }, () {
    //onCancel
  });
}

String getTotalPriceText(List<FoodProduct> products) {
  int sum = 0;
  products.forEach((product) {
    sum += product.getPriceValue();
  });
  return '$sum р.';
}
