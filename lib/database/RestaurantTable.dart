import 'package:food_app/api/FoodRestaurant.dart';
import 'package:sqflite/sqflite.dart';
import 'package:sqflite/sqlite_api.dart';

import 'FoodDatabase.dart';

Future<List<FoodRestaurant>> getRestaurants() async {
  Database db = await DB.database;
  final List<Map<String, dynamic>> maps = await db.query(restaurantTable);

  return List.generate(maps.length, (i) {
    return FoodRestaurant.fromJson(maps[i]);
  });
}

Future<List<FoodRestaurant>> getRestaurantsWithCityId(int cityId) async {
  Database db = await DB.database;
  final List<Map<String, dynamic>> maps =
      await db.rawQuery("SELECT * FROM $restaurantTable WHERE city_id=$cityId");

  return List.generate(maps.length, (i) {
    return FoodRestaurant.fromJson(maps[i]);
  });
}

Future<void> insertRestaurant(FoodRestaurant restaurant) async {
  Database db = await DB.database;
  await db.insert(restaurantTable, restaurant.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace);
}

Future<void> updateRestaurant(FoodRestaurant restaurant) async {
  Database db = await DB.database;
  await db.update(restaurantTable, restaurant.toMap());
}

Future<void> deleteRestaurant(int id) async {
  Database db = await DB.database;
  await db.delete(
    restaurantTable,
    where: "id = ?",
    whereArgs: [id],
  );
}

Future<void> deleteAllRestaurants() async {
  Database db = await DB.database;
  await db.rawDelete("DELETE FROM $restaurantTable");
}

Future<void> cleanUpdateRestaurants(List<FoodRestaurant> restaurants) async {
  await deleteAllRestaurants();
  restaurants.forEach((each) async {
    await insertRestaurant(each);
  });
}
