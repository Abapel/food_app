import 'package:flutter/cupertino.dart';
import 'package:food_app/api/FoodProduct.dart';
import 'package:food_app/events/CountChangedEvent.dart';
import 'package:food_app/events/EventManager.dart';
import 'package:food_app/firebase/FoodAnalytics.dart';
import 'package:sqflite/sqflite.dart';
import 'package:sqflite/sqlite_api.dart';

import 'DatabaseHelper.dart';
import 'FoodDatabase.dart';

Future<List<FoodProduct>> getProducts() async {
  Database db = await DB.database;
  final List<Map<String, dynamic>> maps = await db.query(productsTable);
  final List<FoodProduct> ordered = await getOrderedProducts();

  return List.generate(maps.length, (i) {
    FoodProduct result = FoodProduct.fromJson(maps[i]);
    ordered.forEach((each) {
      if (result.id == each.id) {
        result.count = each.count;
      }
    });
    return result;
  });
}

Future<List<FoodProduct>> getProductsWithRestaurantId(int restaurantId) async {
  Database db = await DB.database;
  final List<Map<String, dynamic>> maps = await db.rawQuery(
      "SELECT * FROM $productsTable WHERE restaurant_id=$restaurantId");
  final List<FoodProduct> ordered = await getOrderedProducts();

  return List.generate(maps.length, (i) {
    FoodProduct result = FoodProduct.fromJson(maps[i]);
    ordered.forEach((each) {
      if (result.id == each.id) {
        result.count = each.count;
      }
    });
    return result;
  });
}

Future<List<FoodProduct>> getProductsWithCategoryId(int categoryId) async {
  Database db = await DB.database;
  final List<Map<String, dynamic>> maps = await db
      .rawQuery("SELECT * FROM $productsTable WHERE category_id=$categoryId");
  final List<FoodProduct> ordered = await getOrderedProducts();

  return List.generate(maps.length, (i) {
    FoodProduct result = FoodProduct.fromJson(maps[i]);
    ordered.forEach((each) {
      if (result.id == each.id) {
        result.count = each.count;
      }
    });
    return result;
  });
}

Future<void> deleteAllOrderedProducts() async {
  Database db = await DB.database;
  await db.rawDelete("DELETE FROM $orderedProductsTable");
  broadcastEvent(0);
}

Future<List<FoodProduct>> getOrderedProducts() async {
  Database db = await DB.database;
  final List<Map<String, dynamic>> maps = await db.query(orderedProductsTable);

  return List.generate(maps.length, (i) {
    return FoodProduct.fromJson(maps[i]);
  });
}

Future<void> insertProduct(FoodProduct product) async {
  Database db = await DB.database;
  await db.insert(productsTable, product.toMap(),
      conflictAlgorithm: ConflictAlgorithm.ignore);
}

Future<void> updateProduct(FoodProduct product) async {
  Database db = await DB.database;
  int result = await db.update(productsTable, product.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace);
  print(result);
}

Future<void> deleteProduct(int id) async {
  Database db = await DB.database;
  await db.delete(
    productsTable,
    where: "id = ?",
    whereArgs: [id],
  );
}

Future<void> deleteAllProducts() async {
  Database db = await DB.database;
  await db.rawDelete("DELETE FROM $productsTable");
}

Future<void> cleanUpdateProducts(List<FoodProduct> products) async {
  await deleteAllProducts();
  products.forEach((each) async {
    await insertProduct(each);
  });
}

Future<bool> hasConflictRestaurantId(FoodProduct product) async {
  List<FoodProduct> orderedProducts = await getOrderedProducts();
  bool conflict = false;
  orderedProducts.forEach((each) {
    if (each.restaurantId != product.restaurantId) {
      conflict = true;
    }
  });
  return conflict;
}

Future<void> setOrderedProductWithErrorAlert(
    BuildContext context, FoodProduct product) async {
  bool success = await setOrderedProduct(product);
  if (!success) {
    showConfirmDeleteOnConflictOrderedProductsDialog(context, () async {
      await setOrderedProduct(product);
    });
  }
}

Future<bool> setOrderedProduct(FoodProduct product) async {
  bool result = true;
  Database db = await DB.database;
  trackProductCount(product.name, product.count);
  if (product.hasItems()) {
    if (await hasConflictRestaurantId(product)) {
      result = false;
    } else {
      await db.insert(orderedProductsTable, product.toMap(),
          conflictAlgorithm: ConflictAlgorithm.replace);
    }
  } else {
    await db
        .delete(orderedProductsTable, where: "id = ?", whereArgs: [product.id]);
  }

  List<FoodProduct> list = await getOrderedProducts();
  print('ORDERED PRODUCTS LIST = ${list.length}');
  list.forEach((each) {
    print('${each.name}: ${each.count}');
  });

  broadcastEvent(list.length);
  return result;
}

Future<int> getOrderedProductCount(int productId) async {
  List<FoodProduct> products = await getOrderedProducts();
  int count = 0;
  products.forEach((each) {
    if (each.id == productId) {
      count = each.count;
    }
  });
  return count;
}

Future<void> removeAllOrderedProducts() async {
  //me
}

void broadcastEvent(int count) {
  EventManager.getInstance().eventBus.fire(CountChangedEvent(count));
}
