import 'dart:io';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:sqflite/sqlite_api.dart';

final String postsTable = 'Posts';
final String restaurantTable = 'Restaurants';
final String productsTable = 'Products';
final String orderedProductsTable = 'OrderedProducts';

class DB {
  DB._();
  static final DB db = DB._();

  static Database _database;
  static Future<Database> get database async {
    if (_database != null) return _database;
    _database = await initDB();
    return _database;
  }

  static initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "food_app.db");
    return openDatabase(path, version: 1, onOpen: (db) {},
        onCreate: (Database db, int version) async {
      await db.execute("CREATE TABLE $postsTable ("
          "id INTEGER PRIMARY KEY,"
          "title TEXT,"
          "announce TEXT,"
          "full_text TEXT,"
          "created_at TEXT,"
          "city_id INTEGER,"
          "image_url TEXT)");
      await db.execute("CREATE TABLE $restaurantTable ("
          "id INTEGER PRIMARY KEY,"
          "city_id INTEGER,"
          "name TEXT,"
          "image_url TEXT,"
          "reviews_count INTEGER,"
          "overall_rating REAL,"
          "begin_working TEXT,"
          "end_working TEXT,"
          "description TEXT,"
          "telephone TEXT,"
          "created_at TEXT,"
          "cuisines TEXT,"
          "reviews TEXT)");
      await db.execute("CREATE TABLE $productsTable ("
          "id INTEGER PRIMARY KEY,"
          "category_id INTEGER,"
          "sort INTEGER,"
          "count INTEGER,"
          "city_id INTEGER,"
          "restaurant_id INTEGER,"
          "category TEXT,"
          "name TEXT,"
          "description TEXT,"
          "price INTEGER,"
          "weight INTEGER,"
          "created_at TEXT,"
          "images TEXT,"
          "tax_value INTEGER)");
      await db.execute("CREATE TABLE $orderedProductsTable ("
          "id INTEGER PRIMARY KEY,"
          "category_id INTEGER,"
          "sort INTEGER,"
          "count INTEGER,"
          "city_id INTEGER,"
          "restaurant_id INTEGER,"
          "category TEXT,"
          "name TEXT,"
          "description TEXT,"
          "price INTEGER,"
          "weight INTEGER,"
          "created_at TEXT,"
          "images TEXT,"
          "tax_value INTEGER)");
    });
  }
}
