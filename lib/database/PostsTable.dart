import 'package:food_app/api/FoodPost.dart';
import 'package:sqflite/sqflite.dart';
import 'package:sqflite/sqlite_api.dart';

import 'FoodDatabase.dart';

Future<List<FoodPost>> getPosts() async {
  Database db = await DB.database;
  final List<Map<String, dynamic>> maps = await db.query(postsTable);

  return List.generate(maps.length, (i) {
    return FoodPost.fromJson(maps[i]);
  });
}

Future<void> insertPost(FoodPost post) async {
  Database db = await DB.database;
  await db.insert(postsTable, post.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace);
}

Future<void> updatePost(FoodPost post) async {
  Database db = await DB.database;
  await db.update(postsTable, post.toMap());
}

Future<void> deletePost(int id) async {
  Database db = await DB.database;
  await db.delete(
    postsTable,
    where: "id = ?",
    whereArgs: [id],
  );
}

Future<void> deleteAllPosts() async {
  Database db = await DB.database;
  await db.rawDelete("DELETE FROM $postsTable");
}

Future<void> cleanUpdatePosts(List<FoodPost> posts) async {
  await deleteAllPosts();
  posts.forEach((each) async {
    await insertPost(each);
  });
}
