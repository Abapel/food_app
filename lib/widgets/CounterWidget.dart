import 'package:flutter/material.dart';
import 'package:food_app/api/FoodProduct.dart';
import 'package:food_app/database/ProductsTable.dart';

// ignore: must_be_immutable
class CounterWidget extends StatefulWidget {
  int count = 0;
  double size = 0;
  VoidCallback callback;
  FoodProduct food;

  CounterWidget(
      {@required int count,
      @required double size,
      @required FoodProduct food,
      @required VoidCallback callback}) {
    this.count = count;
    this.size = size;
    this.food = food;
    this.callback = callback;
  }

  CounterWidgetState state;

  @override
  CounterWidgetState createState() {
    if (state == null) {
      state = CounterWidgetState(
          count: count, size: size, food: food, callback: callback);
    }
    return state;
  }
}

class CounterWidgetState extends State<CounterWidget> {
  int count = 0;
  double size = 0;
  VoidCallback callback;
  FoodProduct food;

  CounterWidgetState(
      {@required int count,
      @required double size,
      @required FoodProduct food,
      @required VoidCallback callback}) {
    this.count = count;
    this.size = size;
    this.food = food;
    this.callback = callback;
  }

  void onMinusClicked() {
    if (!mounted) return;
    setState(() {
      count--;
      if (count < 0) {
        count = 0;
      }
      food.count = count;
      callback();
      broadcastEvent(count);
    });
  }

  void onPlusClicked() {
    if (!mounted) return;
    setState(() {
      count++;
      food.count = count;
      callback();
      broadcastEvent(count);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        SizedBox(
          width: size,
          height: size,
          child: InkWell(
            child: Icon(Icons.remove),
            onTap: () {
              onMinusClicked();
            },
          ),
        ),
        Center(
            child: Padding(
          padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
          child: Text(
            '$count',
            style: TextStyle(fontSize: 20),
          ),
        )),
        SizedBox(
          width: size,
          height: size,
          child: InkWell(
            child: Icon(Icons.add),
            onTap: () {
              onPlusClicked();
            },
          ),
        ),
      ],
    );
  }
}
