import 'package:flutter/material.dart';
import 'package:food_app/api/FoodProduct.dart';
import 'package:food_app/database/ProductsTable.dart';

// ignore: must_be_immutable
class StatelessCounterWidget extends StatelessWidget {
  int count = 0;
  double size = 0;
  VoidCallback callback;
  FoodProduct food;

  StatelessCounterWidget(
      {@required int count,
      @required double size,
      @required FoodProduct food,
      @required VoidCallback callback}) {
    this.count = count;
    this.size = size;
    this.food = food;
    this.callback = callback;
  }

  void onMinusClicked() {
    count--;
    if (count < 0) {
      count = 0;
    }
    food.count = count;
    callback();
    broadcastEvent(count);
  }

  void onPlusClicked() {
    count++;
    food.count = count;
    callback();
    broadcastEvent(count);
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        SizedBox(
          width: size,
          height: size,
          child: InkWell(
            child: Icon(Icons.remove),
            onTap: () {
              onMinusClicked();
            },
          ),
        ),
        Center(
            child: Padding(
          padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
          child: Text(
            '$count',
            style: TextStyle(fontSize: 20),
          ),
        )),
        SizedBox(
          width: size,
          height: size,
          child: InkWell(
            child: Icon(Icons.add),
            onTap: () {
              onPlusClicked();
            },
          ),
        ),
      ],
    );
  }
}
