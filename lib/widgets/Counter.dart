import 'package:flutter/material.dart';
import 'package:food_app/api/FoodProduct.dart';

// ignore: must_be_immutable
class Counter extends StatefulWidget {
  FoodProduct foodProduct;
  Counter(FoodProduct foodProduct) {
    this.foodProduct = foodProduct;
  }

  @override
  CounterState createState() => new CounterState(foodProduct);
}

class CounterState extends State<Counter> {
  FoodProduct foodProduct;
  int count = 0;

  CounterState(FoodProduct foodProduct) {
    this.foodProduct = foodProduct;
    this.count = foodProduct.count;
  }

  void increaseCount() {
    if (!mounted) return;
    setState(() {
      foodProduct.increateCount();
      this.count = foodProduct.count;
    });
  }

  void decrementCount() {
    if (!mounted) return;
    setState(() {
      foodProduct.decreateCount();
      this.count = foodProduct.count;
    });
  }

  Widget build(context) {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.fromLTRB(0, 8, 0, 0),
        child: Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                child: Stack(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Offstage(
                          offstage: count == 0,
                          child: Row(
                            children: <Widget>[
                              Padding(
                                padding: EdgeInsets.fromLTRB(0, 0, 16, 0),
                                child: SizedBox(
                                  width: 50,
                                  child: FlatButton(
                                    child: Icon(Icons.remove),
                                    onPressed: decrementCount,
                                  ),
                                ),
                              ),
                              Center(
                                  child: Text(
                                '$count',
                                style: TextStyle(fontSize: 24),
                              )),
                              Padding(
                                padding: EdgeInsets.fromLTRB(8, 0, 0, 0),
                                child: SizedBox(
                                  width: 50,
                                  child: FlatButton(
                                    child: Icon(Icons.add),
                                    onPressed: increaseCount,
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                        Offstage(
                          offstage: count > 0,
                          child: RaisedButton(
                            onPressed: increaseCount,
                            textColor: Colors.white,
                            child: Row(
                              children: <Widget>[
                                Text(
                                  'Заказать',
                                  style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.w800),
                                )
                              ],
                            ),
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
