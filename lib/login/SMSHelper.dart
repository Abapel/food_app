import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';

class SMSHelper {
  String verificationId;

  VoidCallback onPincodeSent;
  VoidCallback onSignedIn;
  VoidCallback onError;

  SMSHelper({this.onPincodeSent, this.onSignedIn, this.onError});

  void verifyPincode(String pincode) async {
    AuthCredential authCredential = PhoneAuthProvider.getCredential(
        verificationId: this.verificationId, smsCode: pincode);
    await FirebaseAuth.instance
        .signInWithCredential(authCredential)
        .then((AuthResult result) async {
      onSignedIn();
    });
  }

  /// Sends the code to the specified phone number.
  Future<void> sendPinCode(String phoneNumber) async {
    final Function(AuthCredential user) verificationCompleted =
        (AuthCredential user) {
      //onSignedIn();
    };

    final PhoneVerificationFailed verificationFailed =
        (AuthException authException) {
      onError();
    };

    final PhoneCodeSent codeSent =
        (String verificationId, [int forceResendingToken]) async {
      this.verificationId = verificationId;
      onPincodeSent();
    };

    final PhoneCodeAutoRetrievalTimeout codeAutoRetrievalTimeout =
        (String verificationId) {
      this.verificationId = verificationId;
      onError();
    };

    await FirebaseAuth.instance.verifyPhoneNumber(
        phoneNumber: phoneNumber,
        timeout: const Duration(seconds: 5),
        verificationCompleted: verificationCompleted,
        verificationFailed: verificationFailed,
        codeSent: codeSent,
        codeAutoRetrievalTimeout: codeAutoRetrievalTimeout);
  }
}
