import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:food_app/api/API.dart';
import 'package:food_app/database/PostsTable.dart';
import 'package:food_app/database/ProductsTable.dart';
import 'package:food_app/database/RestaurantTable.dart';
import 'package:food_app/navigation/BottomNavigationPage.dart';

class SplashScreen extends StatefulWidget {
  @override
  SplashScreenState createState() => SplashScreenState();
}

Future<void> loadData() async {
  await getRestaurants().then((restaurants) async {
    await API().getRestaurants().then((newRestaurants) async {
      await cleanUpdateRestaurants(newRestaurants);

      await getPosts().then((posts) async {
        await API().getPosts().then((newPosts) async {
          await cleanUpdatePosts(newPosts);

          await getProducts().then((products) async {
            await API().getProducts().then((newProducts) async {
              await cleanUpdateProducts(newProducts);
            });
          });
        });
      });
    });
  });
}

class SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();

    loadData().then((void v) {
      openHomeScreen();
    });
  }

  void openHomeScreen() {
    Navigator.of(context).pushReplacement(MaterialPageRoute(
        builder: (BuildContext context) => BottomNavigationPage()));
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
            backgroundColor: Colors.white,
            body: Stack(
              children: <Widget>[
                SizedBox.expand(
                  child: Image.asset(
                    'images/splash_screen.png',
                    fit: BoxFit.cover,
                  ),
                ),
                Center(
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(0, 150, 0, 0),
                    child: SpinKitCircle(
                      color: Colors.white,
                      size: 20,
                      duration: Duration(milliseconds: 1000),
                    ),
                  ),
                ),
              ],
            )));
  }
}
