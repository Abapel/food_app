import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SignInWidget extends StatefulWidget {
  @override
  SignInWidgetState createState() {
    return SignInWidgetState();
  }
}

class SignInWidgetState extends State<SignInWidget> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Card(
            elevation: 2,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
              child: TextField(
                  decoration: InputDecoration(labelText: 'Имя'),
                  textInputAction: TextInputAction.next),
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(0, 16, 0, 0),
            child: Card(
              elevation: 2,
              child: Padding(
                padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
                child: TextField(
                    decoration: InputDecoration(labelText: 'Email'),
                    textInputAction: TextInputAction.next),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(0, 16, 0, 0),
            child: Card(
              elevation: 2,
              child: Padding(
                padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
                child: TextField(
                    decoration: InputDecoration(labelText: 'Пароль'),
                    textInputAction: TextInputAction.next),
              ),
            ),
          )
        ],
      ),
    );
  }
}
