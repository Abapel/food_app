import 'dart:convert';

import 'package:food_app/api/FoodReview.dart';

List<String> parseListOfStrings(Object object) {
  if (object is String) {
    final jsonData = jsonDecode(object);
    return (jsonData as List).map((i) => i.toString()).toList();
  } else if (object is List) {
    return object.map((i) => i.toString()).toList();
  }
  return <String>[];
}

List<FoodReview> parseFoodReviews(Object object) {
  if (object is String) {
    return (jsonDecode(object) as List)
        .map((i) => FoodReview.fromJson(i))
        .toList();
  } else if (object is List && object.length > 0) {
    return object.map((i) => FoodReview.fromJson(i)).toList();
  }
  return <FoodReview>[];
}
