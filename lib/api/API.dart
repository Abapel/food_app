import 'dart:convert';

import 'package:food_app/api/FoodPost.dart';
import 'package:food_app/api/FoodProduct.dart';
import 'package:food_app/api/FoodRestaurant.dart';
import 'package:http/http.dart' as http;

class API {
  String baseUrl = 'http://admin.lafood.site/api/';
  String news = 'news';
  String products = 'products';
  String orders = 'order';
  String restaurants = 'restaurants';

  Future<List<FoodPost>> getPosts() async {
    final response = await http.get('$baseUrl$news');
    if (response.statusCode == 200) {
      var data = jsonDecode(response.body) as List;
      List<FoodPost> list =
          data.map<FoodPost>((json) => FoodPost.fromJson(json)).toList();
      return list;
    } else {
      throw Exception('Failed to load news');
    }
  }

  Future<List<FoodProduct>> getProducts() async {
    final response = await http.get('$baseUrl$products');
    if (response.statusCode == 200) {
      var data = jsonDecode(response.body) as List;
      List<FoodProduct> list =
          data.map<FoodProduct>((json) => FoodProduct.fromJson(json)).toList();
      return list;
    } else {
      throw Exception('Failed to load products');
    }
  }

  Future<List<FoodRestaurant>> getRestaurants() async {
    final response = await http.get('$baseUrl$restaurants');
    if (response.statusCode == 200) {
      var data = jsonDecode(response.body) as List;
      List<FoodRestaurant> list = data
          .map<FoodRestaurant>((json) => FoodRestaurant.fromJson(json))
          .toList();
      return list;
    } else {
      throw Exception('Failed to load products');
    }
  }

  Future<bool> createOrder({Map body}) async {
    final response = await http.post('$baseUrl$orders', body: body);
    return response.statusCode == 200; //Return true if success
  }
}
