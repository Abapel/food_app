import 'dart:convert';

import 'APIHelper.dart';
import 'FoodCategory.dart';

class FoodProduct {
  static final String keyId = 'id';
  static final String keyProductId = 'product_id';
  static final String keyCategoryId = 'category_id';
  static final String keySort = 'sort';
  static final String keyCityId = 'city_id';
  static final String keyCount = 'count';
  static final String keyRestaurantId = 'restaurant_id';
  static final String keyCategory = 'category';
  static final String keyName = 'name';
  static final String keyDescription = 'description';
  static final String keyPrice = 'price';
  static final String keyWeight = 'weight';
  static final String keyCreatedAt = 'created_at';
  static final String keyImages = 'images';
  static final String keyTaxValue = 'tax_value';

  final int id;
  final int categoryId;
  final int sort;
  int count;
  final int cityId;
  final int restaurantId;
  final FoodCategory category;
  final String name;
  final String description;
  final int price;
  final int weight;
  final String createdAt;
  final List<String> images;
  final int taxValue;

  bool hasItems() {
    return count > 0;
  }

  int getPrice() {
    return price;
  }

  int getCount() {
    if (count != null) {
      return count;
    }
    return 0;
  }

  int getPriceValue() {
    return getCount() * getPrice();
  }

  String getPriceText() {
    int result = getCount() * getPrice();
    if (result == 0) {
      result = getPrice();
    }
    return '$result р.';
  }

  String getImageUrl() {
    if (images.isNotEmpty && images[0] is String) {
      return images[0];
    }
    return '';
  }

  String getWeight() {
    if (weight != null) {
      return '$weight г.';
    }
    return '';
  }

  void increateCount() {
    this.count++;
  }

  void decreateCount() {
    int newValue = this.count - 1;
    if (newValue < 0) {
      newValue = 0;
    }
    this.count = newValue;
  }

  FoodProduct({
    this.id,
    this.categoryId,
    this.sort,
    this.count,
    this.cityId,
    this.restaurantId,
    this.category,
    this.name,
    this.description,
    this.price,
    this.weight,
    this.createdAt,
    this.images,
    this.taxValue,
  });

  factory FoodProduct.fromJson(Map<String, dynamic> json) {
    FoodCategory parseCategory(Object object) {
      if (object is String) {
        return FoodCategory.fromJsonString(object);
      } else if (object is Map) {
        return FoodCategory.fromJson(object);
      } else {
        return object;
      }
    }

    return FoodProduct(
      id: json[keyId],
      categoryId: json[keyCategoryId],
      sort: json[keySort],
      count: json[keyCount] != null ? json[keyCount] : 0,
      cityId: json[keyCityId],
      restaurantId: json[keyRestaurantId],
      category: parseCategory(json[keyCategory]),
      name: json[keyName],
      description: json[keyDescription],
      price: json[keyPrice],
      weight: json[keyWeight],
      createdAt: json[keyCreatedAt],
      images: parseListOfStrings(json[keyImages]),
      taxValue: json[keyTaxValue],
    );
  }

  Map<String, dynamic> toMap() {
    return {
      keyId: id,
      keyCategoryId: categoryId,
      keySort: sort,
      keyCount: count,
      keyCityId: cityId,
      keyRestaurantId: restaurantId,
      keyCategory: category != null ? category.toJson() : '{}',
      keyName: name,
      keyDescription: description,
      keyPrice: price,
      keyWeight: weight,
      keyCreatedAt: createdAt,
      keyImages: jsonEncode(images),
      keyTaxValue: taxValue,
    };
  }

  Map toApiMap() {
    return {keyProductId: id, keyCount: count};
  }
}
