class FoodPost {
  final int id;
  final int cityId;
  final String title;
  final String announce;
  final String fullText;
  final String createdAt;
  final String imageUrl;

  FoodPost(
      {this.id,
      this.title,
      this.announce,
      this.fullText,
      this.createdAt,
      this.cityId,
      this.imageUrl});

  factory FoodPost.fromJson(Map<String, dynamic> json) {
    return FoodPost(
      id: json['id'],
      title: json['title'],
      announce: json['announce'],
      fullText: json['full_text'],
      createdAt: json['created_at'],
      cityId: json['city_id'],
      imageUrl: json['image_url'],
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'title': title,
      'announce': announce,
      'full_text': fullText,
      'created_at': createdAt,
      'city_id': cityId,
      'image_url': imageUrl,
    };
  }
}
