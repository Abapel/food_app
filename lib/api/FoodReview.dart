class FoodReview {
  final int id;
  final int userId;
  final int restaurantId;
  final int rate;
  final String review;

  FoodReview({this.id, this.userId, this.restaurantId, this.rate, this.review});

  factory FoodReview.fromJson(Map<String, dynamic> json) {
    return FoodReview(
      id: json['id'],
      userId: json['user_id'],
      restaurantId: json['restaurant_id'],
      rate: json['rate'],
      review: json['review'],
    );
  }
}
