import 'dart:convert';

import 'package:food_app/api/FoodReview.dart';

import 'APIHelper.dart';

class FoodRestaurant {
  static final String keyId = 'id';
  static final String keyCityId = 'city_id';
  static final String keyName = 'name';
  static final String keyImageUrl = 'image_url';
  static final String keyReviewsCount = 'reviews_count';
  static final String keyOverallRating = 'overall_rating';
  static final String keyBeginWorking = 'begin_working';
  static final String keyEndWorking = 'end_working';
  static final String keyDescription = 'description';
  static final String keyTelephone = 'telephone';
  static final String keyCreatedAt = 'created_at';
  static final String keyCuisines = 'cuisines';
  static final String keyReviews = 'reviews';

  final int id;
  final int cityId;
  final String name;
  final String imageUrl;
  final int reviewsCount;
  final double overallRating;
  final String beginWorking;
  final String endWorking;
  final String description;
  final String telephone;
  final String createdAt;

  final List<String> cuisines;
  final List<FoodReview> reviews;

  String getWorkingTimeText() {
    if (beginWorking.length > 3 && endWorking.length > 3) {
      return 'с ${beginWorking.substring(0, beginWorking.length - 3)} по ${endWorking.substring(0, endWorking.length - 3)}';
    }
    return '';
  }

  FoodRestaurant({
    this.id,
    this.cityId,
    this.name,
    this.imageUrl,
    this.reviewsCount,
    this.overallRating,
    this.beginWorking,
    this.endWorking,
    this.description,
    this.telephone,
    this.createdAt,
    this.cuisines,
    this.reviews,
  });

  factory FoodRestaurant.fromJson(Map<String, dynamic> json) {
    return FoodRestaurant(
        id: json[keyId],
        cityId: json[keyCityId],
        name: json[keyName],
        imageUrl: json[keyImageUrl],
        reviewsCount: json[keyReviewsCount],
        overallRating: json[keyOverallRating].toDouble(),
        beginWorking: json[keyBeginWorking],
        endWorking: json[keyEndWorking],
        description: json[keyDescription],
        telephone: json[keyTelephone],
        createdAt: json[keyCreatedAt],
        cuisines: parseListOfStrings(json[keyCuisines]),
        reviews: <FoodReview>[]);
  }

  Map<String, dynamic> toMap() {
    return {
      keyId: id,
      keyCityId: cityId,
      keyName: name,
      keyImageUrl: imageUrl,
      keyReviewsCount: reviewsCount,
      keyOverallRating: overallRating,
      keyBeginWorking: beginWorking,
      keyEndWorking: endWorking,
      keyDescription: description,
      keyTelephone: telephone,
      keyCreatedAt: createdAt,
      keyCuisines: jsonEncode(cuisines),
      keyReviews: json.encode(reviews)
    };
  }
}
