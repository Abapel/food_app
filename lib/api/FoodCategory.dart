import 'dart:convert';

import 'package:food_app/api/FoodProduct.dart';

class FoodCategory {
  final int id;
  final int parentId;
  final String name;
  final dynamic imageUrl;

  List<FoodProduct> products = <FoodProduct>[];

  String getNormalizedName() {
    return name.replaceFirst(' ', '');
  }

  FoodCategory({this.id, this.parentId, this.name, this.imageUrl});

  factory FoodCategory.fromJson(Map<String, dynamic> json) {
    return FoodCategory(
      id: json['id'],
      parentId: json['parent_id'],
      name: json['name'],
      imageUrl: json['image_url'],
    );
  }

  factory FoodCategory.fromJsonString(String jsonString) {
    final jsonData = json.decode(jsonString);
    return FoodCategory.fromJson(jsonData);
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'parent_id': parentId,
      'name': name,
      'image_url': imageUrl,
    };
  }

  String toJson() {
    return json.encode(toMap());
  }
}
