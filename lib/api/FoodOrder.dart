class FoodOrder {
  final String customerId = "";
  final String name;
  final String address;
  final String phone;
  final String paymentMethod = "cash";
  final String stripeToken = "";
  final String paypalId = "";
  final String promoCode = "";
  final String products;
  final String lat;
  final String lng;

  final String deliveryAreaId = '1';
  final String cityId;
  final String restaurantId;
  final String comment;

  FoodOrder(
      {this.name,
      this.address,
      this.phone,
      this.lat,
      this.lng,
      this.products,
      this.cityId,
      this.restaurantId,
      this.comment});

  factory FoodOrder.fromJson(Map<String, dynamic> json) {
    return FoodOrder(
        name: json['name'],
        address: json['address'],
        phone: json['phone'],
        lat: json['lat'],
        lng: json['lng'],
        products: json['products'],
        cityId: json['city_id'],
        restaurantId: json['restaurant_id'],
        comment: json['comment']);
  }

  Map toMap() {
    var map = new Map<String, dynamic>();
    map["customer_id"] = customerId;
    map["name"] = name;
    map["address"] = address;
    map["phone"] = phone;
    map["lat"] = lat;
    map["lng"] = lng;
    map["products"] = products;
    map["city_id"] = cityId;
    map["restaurant_id"] = restaurantId;
    map["payment_method"] = paymentMethod;
    map["delivery_area_id"] = deliveryAreaId;
    map["stripe_token"] = stripeToken;
    map["comment"] = comment;
    return map;
  }
}
