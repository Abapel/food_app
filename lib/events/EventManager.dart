import 'package:event_bus/event_bus.dart';

class EventManager {
  EventBus eventBus;
  EventManager() {
    eventBus = EventBus();
  }

  static EventManager _manager;
  static EventManager getInstance() {
    if (_manager == null) {
      _manager = new EventManager();
    }
    return _manager;
  }
}
