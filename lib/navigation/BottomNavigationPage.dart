import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:food_app/database/ProductsTable.dart';
import 'package:food_app/events/EventManager.dart';
import 'package:food_app/events/ShowRestaurantsEvent.dart';
import 'package:food_app/firebase/FoodAnalytics.dart';
import 'package:food_app/navigation/TabNavigatorRoutes.dart';
import 'package:food_app/news/NewsPage.dart';
import 'package:food_app/restaurants/RestaurantsPage.dart';
import 'package:food_app/shop/ShopPage.dart';

class BottomNavigationPage extends StatefulWidget {
  @override
  BottomNavigationPageState createState() => BottomNavigationPageState();
}

class BottomNavigationPageState extends State<BottomNavigationPage> {
  static int selectedIndex = 0;

  static List<BottomNavigationBarItem> bottomItems;

  static NewsPage newsPage = NewsPage();
  static ShopPage shopPage = ShopPage();
  static RestaurantsPage restaurantsPage = RestaurantsPage();

  List<Widget> pages = <Widget>[restaurantsPage, newsPage, shopPage];

  void onBottomBarItemClicked(int index) {
    if (!mounted) return;
    if (index == selectedIndex) {
      navigatorKeys[selectedIndex]
          .currentState
          .popUntil((route) => route.isFirst);
    } else {
      setState(() {
        selectedIndex = index;
        shopSelected = selectedIndex == 2;
        trackEventWithParam(
            bottomBarNavigationEvent, getPageLogName(selectedIndex));
      });
    }
  }

  String getPageLogName(int index) {
    switch (index) {
      case 0:
        return 'Рестораны';
      case 1:
        return 'Акции';
      case 2:
        return 'Корзина';
    }
    return '';
  }

  void updateBadge() {
    getOrderedProducts().then((products) {
      if (!mounted) return;
      setState(() {
        badge = products.length;
      });
    });
  }

  @override
  void initState() {
    super.initState();

    updateBadge();

    EventManager.getInstance().eventBus.on().listen((event) {
      updateBadge();
    });
    EventManager.getInstance()
        .eventBus
        .on<ShowRestaurantsEvent>()
        .listen((event) {
      onBottomBarItemClicked(0);
    });
  }

  int badge = 0;
  bool shopSelected = false;

  Map<int, GlobalKey<NavigatorState>> navigatorKeys = {
    0: GlobalKey<NavigatorState>(),
    1: GlobalKey<NavigatorState>(),
    2: GlobalKey<NavigatorState>()
  };

  List<String> navigatorRoutes = [
    TabNavigatorRoutes.restaurants,
    TabNavigatorRoutes.news,
    TabNavigatorRoutes.shop,
  ];

  Widget _buildOffstageNavigator(int index) {
    return Offstage(
      offstage: index != selectedIndex,
      child: TabNavigator(
          navigatorKey: navigatorKeys[index],
          navigationRoute: navigatorRoutes[index]),
    );
  }

  @override
  Widget build(BuildContext context) {
    bottomItems = <BottomNavigationBarItem>[
      BottomNavigationBarItem(
        icon: Icon(
          Icons.restaurant,
        ),
        title: Text('Рестораны'),
      ),
      BottomNavigationBarItem(
        icon: Icon(Icons.star),
        title: Text('Акции'),
      ),
      BottomNavigationBarItem(
        activeIcon: Icon(Icons.shopping_cart),
        icon: Stack(
          children: <Widget>[
            Visibility(
              child: Icon(Icons.shopping_cart),
              visible: badge == 0 || shopSelected,
            ),
            Visibility(
              child: Text(
                '$badge',
                style: TextStyle(fontSize: 18, color: Colors.grey[600]),
              ),
              visible: badge > 0 && !shopSelected,
            )
          ],
        ),
        title: Text(
          'Корзина',
        ),
      ),
    ];

    return WillPopScope(
      onWillPop: () async {
        final isFirstRouteInCurrentTab =
            !await navigatorKeys[selectedIndex].currentState.maybePop();
        if (isFirstRouteInCurrentTab) {
          onBottomBarItemClicked(0);
        }
        return isFirstRouteInCurrentTab;
      },
      child: Scaffold(
        body: Stack(children: <Widget>[
          _buildOffstageNavigator(0),
          _buildOffstageNavigator(1),
          _buildOffstageNavigator(2),
        ]),
        bottomNavigationBar: BottomNavigationBar(
          items: bottomItems,
          currentIndex: selectedIndex,
          onTap: onBottomBarItemClicked,
          showSelectedLabels: true,
          showUnselectedLabels: true,
          selectedItemColor: Theme.of(context).accentColor,
          unselectedItemColor: Colors.grey[400],
          selectedLabelStyle: TextStyle(color: Theme.of(context).accentColor),
          unselectedLabelStyle: TextStyle(color: Colors.grey[600]),
        ),
      ),
    );
  }
}
