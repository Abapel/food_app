import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:food_app/api/FoodRestaurant.dart';
import 'package:food_app/menu/MenuPage.dart';
import 'package:food_app/news/NewsPage.dart';
import 'package:food_app/restaurants/RestaurantsPage.dart';
import 'package:food_app/shop/ShopPage.dart';

class TabNavigatorRoutes {
  static const String root = '/';
  static const String restaurants = '/restaurans';
  static const String restaurant = '/restaurant';
  static const String news = '/news';
  static const String shop = '/shop';
  static const String profile = '/profile';
}

class TabNavigator extends StatelessWidget {
  TabNavigator(
      {this.navigatorKey, this.navigationRoute, this.selectedRestaurant});
  final GlobalKey<NavigatorState> navigatorKey;
  final String navigationRoute;
  FoodRestaurant selectedRestaurant;

  void push(BuildContext context, {int materialIndex: 500}) {
    var routeBuilders = _routeBuilders(context, materialIndex: materialIndex);

    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => routeBuilders[navigationRoute](context)));
  }

  Map<String, WidgetBuilder> _routeBuilders(BuildContext context,
      {int materialIndex: 500}) {
    return {
      TabNavigatorRoutes.restaurants: (context) => RestaurantsPage(),
      TabNavigatorRoutes.restaurant: (context) => MenuPage(selectedRestaurant),
      TabNavigatorRoutes.news: (context) => NewsPage(),
      TabNavigatorRoutes.shop: (context) => ShopPage(),
      TabNavigatorRoutes.profile: (context) => CircularProgressIndicator(),
    };
  }

  @override
  Widget build(BuildContext context) {
    var routeBuilders = _routeBuilders(context);

    return Navigator(
        key: navigatorKey,
        initialRoute: TabNavigatorRoutes.root,
        onGenerateRoute: (routeSettings) {
          return MaterialPageRoute(
              builder: (context) => routeBuilders[navigationRoute](context));
        });
  }
}
