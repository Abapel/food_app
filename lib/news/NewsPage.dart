import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:food_app/api/FoodPost.dart';
import 'package:food_app/database/PostsTable.dart';
import 'package:food_app/styles/TextStyles.dart';

class NewsPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return NewsPageState();
  }
}

class NewsPageState extends State<NewsPage> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          body: Stack(
        children: <Widget>[
          AppBar(
            title: Text(
              'Акции',
              style: appBarTitleStyle,
            ),
            backgroundColor: Colors.transparent,
            elevation: 0,
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(0, kToolbarHeight, 0, 0),
            child: FutureBuilder<List<FoodPost>>(
                future: getPosts(),
                builder: (BuildContext context,
                    AsyncSnapshot<List<FoodPost>> snapshot) {
                  if (snapshot.hasData) {
                    return ListView.builder(
                      itemCount: snapshot.data.length,
                      itemBuilder: (BuildContext context, int index) {
                        FoodPost item = snapshot.data[index];
                        return Card(
                          elevation: 2,
                          margin: EdgeInsets.fromLTRB(16, 10, 16, 10),
                          child: Column(
                            children: <Widget>[
                              Image.network(
                                item.imageUrl,
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Container(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.stretch,
                                    children: <Widget>[
                                      Text(item.title,
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 24,
                                              fontWeight: FontWeight.w500)),
                                      Text(
                                        item.createdAt,
                                        style: TextStyle(
                                            color:
                                                Colors.black.withOpacity(0.5),
                                            fontSize: 14),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.fromLTRB(
                                            0, 16, 0, 0),
                                        child: Text(
                                          item.announce,
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 16),
                                        ),
                                      ),
                                      Text(
                                        item.fullText,
                                        style: TextStyle(
                                            color: Colors.black, fontSize: 16),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        );
                      },
                    );
                  } else {
                    return SpinKitCircle(
                      color: Colors.black,
                      size: 22,
                      duration: Duration(milliseconds: 1000),
                    );
                  }
                }),
          ),
        ],
      )),
    );
  }
}
