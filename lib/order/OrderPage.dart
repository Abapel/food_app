import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:food_app/api/API.dart';
import 'package:food_app/api/FoodOrder.dart';
import 'package:food_app/api/FoodProduct.dart';
import 'package:food_app/database/DatabaseHelper.dart';
import 'package:food_app/database/ProductsTable.dart';
import 'package:food_app/dialogs/DialogHelper.dart';
import 'package:food_app/events/EventManager.dart';
import 'package:food_app/events/ShowRestaurantsEvent.dart';
import 'package:food_app/firebase/FoodAnalytics.dart';
import 'package:food_app/styles/TextStyles.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:location/location.dart';
import 'package:geocoder/geocoder.dart';

// ignore: must_be_immutable
class OrderPage extends StatefulWidget {
  List<FoodProduct> products;
  OrderPage({this.products});

  @override
  OrderPageState createState() {
    return OrderPageState(products: products);
  }
}

class OrderPageState extends State<OrderPage> {
  List<FoodProduct> products;
  OrderPageState({this.products});

  final nameController = TextEditingController();
  final phoneController = MaskedTextController(mask: '+0 (000) 000-00-00');
  final emailController = TextEditingController();
  final addressController = TextEditingController();
  final personsController = TextEditingController();
  final commentController = TextEditingController();
  final paymentTypeController = TextEditingController();
  final paymentAmountController = TextEditingController();

  final FocusNode nameFocus = FocusNode();
  final FocusNode phoneFocus = FocusNode();
  final FocusNode emailFocus = FocusNode();
  final FocusNode addressFocus = FocusNode();
  final FocusNode personsFocus = FocusNode();
  final FocusNode commentFocus = FocusNode();

  final String nameErrorText = 'Имя не должно быть пустым';
  final String phoneErrorText = 'Неправильный номер телефона';
  final String emailErrorText = 'Неправильный Email адрес';
  final String addressErrorText = 'Не указан адрес доставки';

  String nameError;
  String phoneError;
  String emailError;
  String addressError;

  var location = Location();
  var latitude = 0.0;
  var longitude = 0.0;
  var loadingLocation = false;
  var termsAccepted = true;

  Future showUserTerms() async {
    const url =
        'https://docs.google.com/document/d/1OCZjwXhj-uP8Dw81iEVNDQdjbZ37S9LfHFBmV0IJzfY/edit?usp=sharing';
    if (await canLaunch(url)) {
      await launch(url);
    }
  }

  int getCityId() {
    if (products.isNotEmpty) {
      return products[0].cityId;
    }
    return 0;
  }

  int getRestaurantId() {
    if (products.isNotEmpty) {
      return products[0].restaurantId;
    }
    return 0;
  }

  String getCommentWithPersonsCount() {
    String result = commentController.text;
    if (personsController.text.isNotEmpty) {
      result += ' Количество персон: ${personsController.text}';
    }
    return result;
  }

  void onCreateOrderClicked() {
    if (isFieldValidationSuccess()) {
    } else {
      //show errors
      setState(() {
        trackEventWithParam(
            creatingOrderErrorEvent, '$nameError $phoneError $addressError');
      });
      return;
    }

    if (products == null) {
      return;
    }

    List<Map<dynamic, dynamic>> orderProducts =
        List.generate(products.length, (i) {
      return products[i].toApiMap();
    });

    FoodOrder order = FoodOrder(
        name: nameController.text,
        address: addressController.text,
        phone: phoneController.text,
        lat: '$latitude',
        lng: '$longitude',
        products: jsonEncode(orderProducts),
        cityId: '${getCityId()}',
        restaurantId: '${getRestaurantId()}',
        comment: getCommentWithPersonsCount());

    API().createOrder(body: order.toMap()).then((success) {
      if (success) {
        trackEventWithParam(orderCreatedEvent, getTotalPriceText(products));
        showAcceptDialog(context, 'Заказ успешно создан',
            'Операторы свяжутся с вами в ближайшее время', 'Закрыть', () async {
          await deleteAllOrderedProducts();
          EventManager.getInstance().eventBus.fire(ShowRestaurantsEvent());
          Navigator.pop(context);
        });
      } else {
        showAcceptCancelDialog(
            context,
            'Произошла ошибка',
            'Не удалось создать заказ. Попробуйте еще раз.',
            'Создать заказ',
            'Отмена', () {
          //onAccept
          onCreateOrderClicked();
        }, () {
          //onCancel
        });
      }
    });
  }

  @override
  void dispose() {
    phoneController.dispose();
    nameFocus.dispose();
    emailFocus.dispose();
    phoneFocus.dispose();
    addressFocus.dispose();
    personsFocus.dispose();
    commentFocus.dispose();
    super.dispose();
  }

  setFocus(BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }

  bool isEmailValid(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    return regex.hasMatch(value);
  }

  bool isFieldValidationSuccess() {
    bool result = true;
    if (nameController.text.isEmpty) {
      nameError = nameErrorText;
      result = false;
    }
    if (phoneController.text.length != 18) {
      phoneError = phoneErrorText;
      result = false;
    }
    /*
    if (!isEmailValid(emailController.text)) {
      emailError = emailErrorText;
      result = false;
    }
     */
    if (addressController.text.isEmpty) {
      addressError = addressErrorText;
      result = false;
    }
    return result;
  }

  Future<bool> isLocationAvailable() async {
    bool gpsServiceEnabled = await location.serviceEnabled();
    if (!gpsServiceEnabled) {
      bool requestAccepted = await location.requestService();
      if (!requestAccepted) {
        return false;
      }
    }
    bool hasGpsPermission = await location.hasPermission();
    if (!hasGpsPermission) {
      bool requestAccepted = await location.requestPermission();
      if (requestAccepted) {
        trackEvent(locationPermissionsGrantedEvent);
      } else {
        trackEvent(locationPermissionsDeniedEvent);
        return false;
      }
    }
    return true;
  }

  void decodeAddress() async {
    final coordinates = new Coordinates(latitude, longitude);
    List<Address> addresses =
        await Geocoder.local.findAddressesFromCoordinates(coordinates);
    setState(() {
      if (addresses.isNotEmpty) {
        addressController.text = addresses[0].addressLine;
      }
      loadingLocation = false;
    });
  }

  void onDetectLocationClicked() async {
    setState(() {
      loadingLocation = true;
      trackEvent(useLocationEvent);
    });

    bool locationAvailable = await isLocationAvailable();
    if (locationAvailable) {
      LocationData userLocation = await location.getLocation();
      this.latitude = userLocation.latitude;
      this.longitude = userLocation.longitude;
      decodeAddress();
      return;
    }

    setState(() {
      loadingLocation = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    paymentTypeController.text = 'Наличными';
    paymentAmountController.text = getTotalPriceText(products);

    return Scaffold(
        backgroundColor: Colors.white,
        body: Stack(
          children: <Widget>[
            AppBar(
              title: Text(
                'Оформление заказа',
                style: appBarTitleStyle,
              ),
              backgroundColor: Colors.white,
              elevation: 0,
              iconTheme: IconThemeData(
                color: Colors.black, //change your color here
              ),
            ),
            Padding(
              padding:
                  const EdgeInsets.fromLTRB(16, kToolbarHeight + 24, 16, 16),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.fromLTRB(0, 0, 0, 16),
                      child: TextField(
                        focusNode: nameFocus,
                        onChanged: (value) {
                          setState(() {
                            nameError = null;
                          });
                        },
                        onSubmitted: (value) {
                          setFocus(context, nameFocus, phoneFocus);
                        },
                        decoration: InputDecoration(
                            labelText: 'Имя',
                            errorText: nameError,
                            prefixIcon: Padding(
                              padding: const EdgeInsets.fromLTRB(0, 0, 16, 0),
                              child: Icon(
                                Icons.person,
                              ),
                            )),
                        textInputAction: TextInputAction.next,
                        controller: nameController,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(0, 0, 0, 16),
                      child: TextField(
                        focusNode: phoneFocus,
                        onChanged: (value) {
                          setState(() {
                            phoneError = null;
                          });
                        },
                        onSubmitted: (value) {
                          setFocus(context, phoneFocus, emailFocus);
                        },
                        decoration: InputDecoration(
                            labelText: 'Телефон',
                            errorText: phoneError,
                            prefixIcon: Padding(
                              padding: const EdgeInsets.fromLTRB(0, 0, 16, 0),
                              child: Icon(
                                Icons.phone_android,
                              ),
                            )),
                        textInputAction: TextInputAction.next,
                        keyboardType: TextInputType.number,
                        controller: phoneController,
                      ),
                    ),
                    /*
                    Padding(
                      padding: const EdgeInsets.fromLTRB(0, 0, 0, 16),
                      child: TextField(
                        focusNode: emailFocus,
                        onChanged: (value) {
                          setState(() {
                            emailError = null;
                          });
                        },
                        onSubmitted: (value) {
                          setFocus(context, emailFocus, addressFocus);
                        },
                        decoration: InputDecoration(
                            labelText: 'Email',
                            errorText: emailError,
                            prefixIcon: Padding(
                              padding: const EdgeInsets.fromLTRB(0, 0, 16, 0),
                              child: Icon(
                                Icons.alternate_email,
                              ),
                            )),
                        textInputAction: TextInputAction.next,
                        keyboardType: TextInputType.emailAddress,
                        controller: emailController,
                      ),
                    ),
                     */
                    Padding(
                      padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                      child: TextField(
                        focusNode: addressFocus,
                        onChanged: (value) {
                          setState(() {
                            addressError = null;
                          });
                        },
                        onSubmitted: (value) {
                          setFocus(context, addressFocus, personsFocus);
                        },
                        decoration: InputDecoration(
                            labelText: 'Адрес',
                            errorText: addressError,
                            prefixIcon: Padding(
                              padding: const EdgeInsets.fromLTRB(0, 0, 16, 0),
                              child: Icon(
                                Icons.location_on,
                              ),
                            )),
                        textInputAction: TextInputAction.next,
                        controller: addressController,
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Offstage(
                          offstage: !loadingLocation,
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
                            child: SpinKitThreeBounce(
                              color: Colors.grey,
                              size: 20,
                              duration: Duration(milliseconds: 1000),
                            ),
                          ),
                        ),
                        Visibility(
                          visible: !loadingLocation,
                          child: InkWell(
                            child: Padding(
                              padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
                              child: Text(
                                'Использовать текущее местоположение',
                                style:
                                    TextStyle(fontSize: 12, color: Colors.red),
                              ),
                            ),
                            onTap: () {
                              if (!loadingLocation) {
                                onDetectLocationClicked();
                              }
                            },
                          ),
                        ),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(0, 16, 0, 0),
                      child: TextField(
                        focusNode: personsFocus,
                        onSubmitted: (value) {
                          setFocus(context, personsFocus, commentFocus);
                        },
                        decoration: InputDecoration(
                            labelText: 'Количество персон',
                            prefixIcon: Padding(
                              padding: const EdgeInsets.fromLTRB(0, 0, 16, 0),
                              child: Icon(
                                Icons.people,
                              ),
                            )),
                        keyboardType: TextInputType.number,
                        textInputAction: TextInputAction.next,
                        controller: personsController,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(0, 16, 0, 0),
                      child: TextField(
                        focusNode: commentFocus,
                        onSubmitted: (value) {
                          commentFocus.unfocus();
                        },
                        decoration: InputDecoration(
                            labelText: 'Комментарий к заказу',
                            prefixIcon: Padding(
                              padding: const EdgeInsets.fromLTRB(0, 0, 16, 0),
                              child: Icon(
                                Icons.comment,
                              ),
                            )),
                        textInputAction: TextInputAction.done,
                        controller: commentController,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(0, 16, 0, 0),
                      child: TextField(
                        decoration: InputDecoration(
                            labelText: 'Оплата',
                            prefixIcon: Padding(
                              padding: const EdgeInsets.fromLTRB(0, 0, 16, 0),
                              child: Icon(
                                Icons.payment,
                              ),
                            )),
                        controller: paymentTypeController,
                        enabled: false,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(0, 16, 0, 0),
                      child: TextField(
                        decoration: InputDecoration(
                            labelText: 'К оплате',
                            prefixIcon: Padding(
                              padding: const EdgeInsets.fromLTRB(0, 0, 16, 0),
                              child: Icon(
                                Icons.local_parking,
                              ),
                            )),
                        controller: paymentAmountController,
                        enabled: false,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(0, 24, 0, 0),
                      child: Row(
                        children: <Widget>[
                          RichText(
                            text: TextSpan(
                                text: 'Я принимаю условия',
                                style: TextStyle(
                                    color: Colors.black, fontSize: 16),
                                children: <TextSpan>[
                                  TextSpan(
                                    recognizer: TapGestureRecognizer()
                                      ..onTap = () {
                                        showUserTerms();
                                      },
                                    text: '\nПользовательского соглашения',
                                    style: TextStyle(
                                        color: Theme.of(context).accentColor,
                                        fontSize: 16),
                                  )
                                ]),
                          )
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(0, 24, 0, 0),
                      child: SizedBox(
                        height: 50,
                        child: RaisedButton(
                          child: Text(
                            'Заказать',
                            style: TextStyle(fontSize: 24, color: Colors.white),
                          ),
                          onPressed: termsAccepted
                              ? () => onCreateOrderClicked()
                              : null,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ));
  }
}
