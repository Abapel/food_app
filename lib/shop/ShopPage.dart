import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:food_app/api/FoodProduct.dart';
import 'package:food_app/database/DatabaseHelper.dart';
import 'package:food_app/database/ProductsTable.dart';
import 'package:food_app/events/EventManager.dart';
import 'package:food_app/events/ShowRestaurantsEvent.dart';
import 'package:food_app/firebase/FoodAnalytics.dart';
import 'package:food_app/order/OrderPage.dart';
import 'package:food_app/styles/TextStyles.dart';
import 'package:food_app/widgets/StatelessCounterWidget.dart';

class ShopPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return ShopPageState();
  }
}

class ShopPageState extends State<ShopPage> {
  static const double bottomBarHeight = 60;

  @override
  void initState() {
    super.initState();
  }

  List<FoodProduct> products = <FoodProduct>[];

  Future<List<FoodProduct>> getProducts() async {
    List<FoodProduct> list = await getOrderedProducts();
    setState(() {
      products = list;
    });
    return list;
  }

  Widget emptyView() {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Icon(
            Icons.remove_shopping_cart,
            size: 35,
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(0, 8, 0, 0),
            child: Text(
              'В корзине ничего нет',
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
              textAlign: TextAlign.center,
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
            child: FlatButton(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(
                    Icons.add_shopping_cart,
                    color: Theme.of(context).primaryColor,
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(8, 0, 0, 0),
                    child: Text(
                      'Выбрать ресторан',
                      style: TextStyle(
                          fontSize: 18, color: Theme.of(context).primaryColor),
                    ),
                  )
                ],
              ),
              onPressed: () {
                EventManager.getInstance()
                    .eventBus
                    .fire(ShowRestaurantsEvent());
              },
            ),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          body: Stack(
        children: <Widget>[
          AppBar(
            title: Text(
              'Корзина',
              style: appBarTitleStyle,
            ),
            backgroundColor: Colors.white,
            elevation: 0,
            actions: <Widget>[
              Offstage(
                offstage: products.length == 0,
                child: InkWell(
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
                    child: Icon(
                      Icons.delete,
                      color: Colors.black,
                    ),
                  ),
                  onTap: () {
                    showConfirmDeleteOrderedProductsDialog(context);
                  },
                ),
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(
                0, kToolbarHeight, 0, bottomBarHeight),
            child: FutureBuilder<List<FoodProduct>>(
              future: getProducts(),
              builder: (BuildContext context,
                  AsyncSnapshot<List<FoodProduct>> snapshot) {
                if (snapshot.hasData) {
                  if (snapshot.data.isEmpty) {
                    return emptyView();
                  } else {
                    return ListView.builder(
                      itemCount: snapshot.data.length,
                      itemBuilder: (context, index) {
                        FoodProduct item = snapshot.data[index];
                        return Padding(
                          padding: EdgeInsets.fromLTRB(16, 16, 16, 16),
                          child: Stack(
                            children: <Widget>[
                              SizedBox(
                                width: 100,
                                height: 100,
                                child: Image.network(
                                  item.getImageUrl(),
                                  fit: BoxFit.cover,
                                ),
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.fromLTRB(116, 0, 0, 0),
                                child: Stack(
                                  children: <Widget>[
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Text(
                                          item.name,
                                          style: TextStyle(fontSize: 18),
                                        ),
                                        Text(
                                          item.category.getNormalizedName(),
                                          style: TextStyle(
                                              fontSize: 16,
                                              color: Colors.grey[500]),
                                        ),
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            StatelessCounterWidget(
                                              count: item.count,
                                              size: 30,
                                              food: item,
                                              callback: () {
                                                if (!mounted) return;
                                                setOrderedProductWithErrorAlert(
                                                    context, item);
                                              },
                                            ),
                                            Text(
                                              item.getPriceText(),
                                              style: TextStyle(fontSize: 20),
                                            )
                                          ],
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                        );
                      },
                    );
                  }
                } else {
                  return SpinKitCircle(
                    color: Colors.black,
                    size: 22,
                    duration: Duration(milliseconds: 1000),
                  );
                }
              },
            ),
          ),
          Offstage(
            offstage: products.length == 0,
            child: Align(
              alignment: Alignment.bottomCenter,
              child: SizedBox(
                height: bottomBarHeight,
                child: Container(
                  color: Colors.grey[200],
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.fromLTRB(16, 8, 0, 8),
                        child: Text(
                          getTotalPriceText(products),
                          style: TextStyle(fontSize: 26),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(0, 8, 16, 8),
                        child: RaisedButton(
                          onPressed: () {
                            trackEventWithParam(creatingOrderEvent,
                                getTotalPriceText(products));
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => OrderPage(
                                        products: products,
                                      )),
                            );
                          },
                          textColor: Colors.white,
                          child: Row(
                            children: <Widget>[
                              Text(
                                'Оформить заказ',
                                style: TextStyle(
                                    fontSize: 20, fontWeight: FontWeight.w800),
                              )
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          )
        ],
      )),
    );
  }
}
