import 'package:firebase_analytics/observer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_crashlytics/flutter_crashlytics.dart';
import 'package:food_app/firebase/FoodAnalytics.dart';
import 'package:food_app/login/SplashScreen.dart';

class FoodApp extends StatelessWidget {
  FoodApp() {
    FlutterCrashlytics().initialize();
  }

  @override
  Widget build(BuildContext context) {
    Color primaryColor = Colors.red;
    Color cursorColor = Colors.white;

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      navigatorObservers: [
        FirebaseAnalyticsObserver(analytics: analytics),
      ],
      theme: ThemeData(
          cursorColor: cursorColor,
          cupertinoOverrideTheme: CupertinoThemeData(
            primaryColor: cursorColor,
          ),
          primaryColor: primaryColor,
          primarySwatch: primaryColor,
          buttonColor: primaryColor,
          accentColor: primaryColor,
          //primaryTextTheme: TextTheme(title: TextStyle(color: Colors.black)),
          fontFamily: 'Helvetica Neue'),
      home: SplashScreen(),
    );
  }
}
