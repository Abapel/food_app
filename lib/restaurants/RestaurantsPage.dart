import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:food_app/api/FoodCity.dart';
import 'package:food_app/api/FoodRestaurant.dart';
import 'package:food_app/database/RestaurantTable.dart';
import 'package:food_app/firebase/FoodAnalytics.dart';
import 'package:food_app/menu/MenuPage.dart';
import 'package:food_app/styles/TextStyles.dart';

class RestaurantsPage extends StatefulWidget {
  @override
  RestaurantsPageState createState() {
    return RestaurantsPageState();
  }
}

class RestaurantsPageState extends State<RestaurantsPage> {
  int selectedCityId;

  Widget getDropDownItemWidget(String text) {
    return Row(
      children: <Widget>[
        Icon(
          Icons.location_on,
          color: Colors.grey,
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(8, 0, 0, 0),
          child: Text(text),
        )
      ],
    );
  }

  List<FoodCity> getCities() {
    return <FoodCity>[
      FoodCity(id: 1, name: 'Севастополь'),
      FoodCity(id: 5, name: 'Симферополь'),
      FoodCity(id: 4, name: 'Евпатория'),
      //FoodCity(id: 6, name: 'Developer')
    ];
  }

  FoodCity getCityWithId(int id) {
    return getCities().firstWhere((x) => x.id == id);
  }

  Widget getCityPicker() {
    List<FoodCity> cities = getCities();
    List<DropdownMenuItem> items = <DropdownMenuItem<int>>[];
    cities.forEach((city) {
      items.add(DropdownMenuItem<int>(
        value: city.id,
        child: getDropDownItemWidget(city.name),
      ));
    });
    return DropdownButton<int>(
      underline: SizedBox(),
      items: items,
      onChanged: (value) {
        setState(() {
          selectedCityId = value;
          trackEventWithParam(
              cityNavigationEvent, getCityWithId(selectedCityId).name);
        });
      },
      value: selectedCityId,
    );
  }

  @override
  void initState() {
    super.initState();
    selectedCityId = getCities()[0].id;
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          backgroundColor: Colors.white,
          body: Stack(
            children: <Widget>[
              AppBar(
                title: Text(
                  'Рестораны',
                  style: appBarTitleStyle,
                ),
                actions: <Widget>[Center(child: getCityPicker())],
                backgroundColor: Colors.white,
                elevation: 0,
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(0, kToolbarHeight, 0, 0),
                child: FutureBuilder<List<FoodRestaurant>>(
                    future: getRestaurantsWithCityId(selectedCityId),
                    builder: (BuildContext context,
                        AsyncSnapshot<List<FoodRestaurant>> snapshot) {
                      if (snapshot.hasData) {
                        double iconSize = 20;

                        return ListView.builder(
                          itemCount: snapshot.data.length,
                          itemBuilder: (BuildContext context, int index) {
                            FoodRestaurant item = snapshot.data[index];
                            return InkWell(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => MenuPage(item)));
                              },
                              child: Card(
                                elevation: 2,
                                margin: EdgeInsets.fromLTRB(16, 10, 16, 10),
                                child: Column(
                                  children: <Widget>[
                                    FadeInImage.assetNetwork(
                                      placeholder: 'images/placeholder.png',
                                      image: item.imageUrl,
                                      height: 100,
                                      width: MediaQuery.of(context).size.width,
                                      fit: BoxFit.cover,
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Container(
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.stretch,
                                          children: <Widget>[
                                            Text(item.name,
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 24,
                                                    fontWeight:
                                                        FontWeight.w500)),
                                            Text(
                                              item.description,
                                              style: TextStyle(
                                                  color: Colors.black
                                                      .withOpacity(0.5),
                                                  fontSize: 14),
                                            ),
                                            Padding(
                                              padding:
                                                  const EdgeInsets.fromLTRB(
                                                      0, 4, 0, 0),
                                              child: Text(
                                                'Работает',
                                                style: TextStyle(
                                                    color: Colors.green,
                                                    fontSize: 14),
                                              ),
                                            ),
                                            Padding(
                                                padding:
                                                    const EdgeInsets.fromLTRB(
                                                        0, 4, 0, 0),
                                                child: Column(
                                                  children: <Widget>[
                                                    Row(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .center,
                                                      children: <Widget>[
                                                        Icon(
                                                          CupertinoIcons.clock,
                                                          size: iconSize,
                                                        ),
                                                        Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                      .fromLTRB(
                                                                  8, 0, 0, 0),
                                                          child: Text(item
                                                              .getWorkingTimeText()),
                                                        )
                                                      ],
                                                    ),
                                                    Padding(
                                                      padding: const EdgeInsets
                                                          .fromLTRB(0, 4, 0, 0),
                                                      child: Row(
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .center,
                                                        children: <Widget>[
                                                          Icon(
                                                            CupertinoIcons.car,
                                                            size: iconSize,
                                                          ),
                                                          Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                        .fromLTRB(
                                                                    8, 0, 0, 0),
                                                            child: Text(
                                                                '90 минут'),
                                                          )
                                                        ],
                                                      ),
                                                    ),
                                                    Padding(
                                                      padding: const EdgeInsets
                                                          .fromLTRB(0, 4, 0, 0),
                                                      child: Row(
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .center,
                                                        children: <Widget>[
                                                          Icon(
                                                            CupertinoIcons
                                                                .conversation_bubble,
                                                            size: iconSize,
                                                          ),
                                                          Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                        .fromLTRB(
                                                                    8, 0, 0, 0),
                                                            child: Text(
                                                                'нет отзывов'),
                                                          )
                                                        ],
                                                      ),
                                                    ),
                                                  ],
                                                ))
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            );
                          },
                        );
                      } else {
                        return Center(
                            child: SpinKitCircle(
                          color: Colors.black,
                          size: 22,
                          duration: Duration(milliseconds: 1000),
                        ));
                      }
                    }),
              ),
            ],
          )),
    );
  }
}
